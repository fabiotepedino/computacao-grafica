# Projeto por Fabio Tepedino 

from Ponto import *

# Plota uma reta tracejada no buffer ativo, via funcao Ponto()
def Reta_Tracejada(x1,y1,x2,y2,espessura = 1, cor="white", tamanho_tracejado = 5):
    x = x1
    y = y1
    p = 0
    dX = x2-x1
    dY = y2-y1
    xInc = 1
    yInc = 1

    tracejado = tamanho_tracejado
    offset = tracejado * 2

    if dX<0 :
        xInc = -1; dX = -dX
    if dY<0 :
        yInc = -1; dY = -dY
    if dY <= dX:
        p = dX/2
        i = 0
        # A primitiva eh igual a reta, mas ignora os espacos que devem ser vazios entre as linhas
        # o offset eh o tamanho da linha + espaco vazio
        # Ex: O tracejado é 5, sao printados 5 pontos, e sao deixados 5 espacos vazios a seguir
        # ao final do espaco vazio, o incremento zera, retornando ao estado anterior.
        while x != x2:
            # Se o incremento i estiver abaixo do valor do tracejado, 
            # o pronto eh printado na tela
            # Caso contrário o ponto é ignorado
            if i <= tamanho_tracejado:
                Ponto(x,y,espessura, cor)
            p=p-dY
            if p<0 :
                y = y+yInc
                p=p+dX
            x=x+xInc
            # Quando o valor do incremento chega ao valor do offset das linhas
            # o incremento eh zerado para que o proximo ponto seja printado.
            if(i == offset):
                i = -1
            i += 1
    else:
        p = dY/2
        i = 0
        # O mesmo caso descrito acima ocorre aqui.
        while y != y2:
            if i <= tamanho_tracejado:
                Ponto(x,y,espessura, cor)
            p = p-dX
            if p<0:
                x = x+xInc
                p = p+dY
            y = y+yInc
            if(i == offset):
                i = -1
            i += 1
        Ponto(x,y,espessura, cor)    
    return
