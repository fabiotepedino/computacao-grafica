# Projeto por Fabio Tepedino 

from Reta import *
from texto import *
import math
from Ponto import *

SuperVerde = color_rgb(94, 235, 52)             # Cria uma cor verde muito forte para ser usada noa avioes            

listaAvioes = []   # Deixar a lista de icones Acessivel a todo o file
listaTextos = []   # Guardar os textos dos avioes para serem apagados no proximo frame
anguloIcones = 0    # Inicializa o angulo a ser incrementado, para a criacao dos icones.

# Projeta o ponto
def projetar( x,  y,z,f=1000,F = 50000):
    x1 = x * (f/(F-z))
    y1 = y * (f/(F-z))
    pontos = [x1, y1]
    return pontos

# Rotaciona pontos em um plano 2D. Usado na criacao dos icones.
def rotacionarPonto(x,y,angulo):
    xSquared = math.pow(x,2)
    ySquared = math.pow(y,2)
    L = math.sqrt(xSquared + ySquared)
    L = math.ceil(L)
    
    alpha = math.radians(90)

    if(x != 0):
        aux = y / x
        alpha = math.atan(aux)
    angulo = math.radians(angulo) + alpha

    x2 = L * math.cos(angulo)   
    x2 = round(x2)

    y2 = L * math.sin(angulo)   
    y2 = round(y2)
    
    pontos = [x2,y2]
    return pontos

# Cria os icones dos avioes.
# Gera 360/angle quantidade de icones.
# Ex: 360/15 = 24 icones
def createAircraftIcons(angle):
    global anguloIcones
    anguloIcones = angle

    # Pontos do aviao base.
    # Sao guardados em sequencia x,y.
    # As retas sao desenhadas de dois em dois pontos
    #Ex: [0][1] ate [2][3] = uma reta.
    pontosAviaoBase = [0, 0, 10, 0, 3, 4, 3, -4, 8, 2, 8, -2]

    # Todos os icones serao adicionados a esta lista
    listaAvioes.append(pontosAviaoBase)

    qtdicones = int(360/angle)
    
    for i in range(qtdicones-1):                                            
        angulo = angle * (i+1)                                              
        pontosAviao = [0,0]                                                 
        # Comeca em 2 por ja termos 2 pontos nos index [0] = 0, [1] = 0
        j = 2
        while j < len(pontosAviaoBase):                                     
            pontos = [pontosAviaoBase[j], pontosAviaoBase[j+1]]             
            pontos = rotacionarPonto(pontos[0], pontos[1], angulo)
            pontosAviao.append(pontos[0])
            pontosAviao.append(pontos[1])
            # Por adicionar 2 pontos em sequencia, temos que pular dois index.
            j += 2                                                          
        listaAvioes.append(pontosAviao)     
    return

# Com os icones prontos, pode-se plota-los no buffer ativo
def printIcons(iconNumber, x, y, cor):
    icon = listaAvioes[iconNumber]
    Reta(icon[0]+x, icon[1]+y, icon[2]+x, icon[3]+y, 1, cor)
    Reta(icon[4]+x, icon[5]+y, icon[6]+x, icon[7]+y, 1, cor)
    Reta(icon[8]+x, icon[9]+y, icon[10]+x, icon[11]+y, 1, cor)
    return

# Seleciona a cor que deve ser printado o aviao
def seletorCor(Status):
    if Status == "P":
        cor = color_rgb(255,0,0)
    elif Status == "D":
        cor = SuperVerde
    else:
        cor = "white"
    return cor

# Seleciona o icone correto, em relacao a posicao do aviao.
def iconSelector(x,y, status):
    if x != 0 :
        # Seleciona o quadrante do aviao
        if x > 0 and y > 0:
            alpha = math.atan(y/x)
        elif x < 0 and y > 0:
            alpha = math.pi + math.atan(y/x)
        elif x < 0 and y < 0:
            alpha = math.pi + math.atan(y/x)
        elif x > 0 and y < 0:
            alpha = 2*math.pi - math.atan(y/x)
    # Caso x = 0 e y > 0. Alpha = 90 graus
    elif y > 0:
        alpha = math.atan(math.pi/2)        
    # Caso x = 0 e y < 0. Alpha = 270 graus
    else:
        alpha = math.atan(3*math.pi/2)
    # Caso esteja decolando, o vetor eh inverto.
    # Giramos 180 graus o aviao, para "fugir do aeroporto"
    if(status == "D"):
        alpha = alpha + math.pi
    # n eh o numero do icone que melhor se aproxima do angulo do aviao em sua posicao atual.
    n = math.degrees(alpha) / anguloIcones
    return round(n)

# Funcao que chama algumas das anteriores, criando o aviao e inserindo no buffer.
def aviao(status, voo, distancia, velocidade, x, y, z):
    
    pontos = projetar(x,y,z)
    XFinal = pontos[0]
    YFinal = pontos[1]

    iconNumber = iconSelector(int(XFinal), int(YFinal), status)
    cor = seletorCor(status)

    printIcons(iconNumber, XFinal, YFinal, cor)
    listaTextos.append(textoRetorno(win, correctionX( XFinal + 15),correctionY( YFinal + 15), voo, 10, cor, "normal"))
    return

# Eh necessario apagar todos os textos que acompanham os avioes, uma vez por frame. 
# Limitacoes da biblioteca
def apagarTextosAvioes():
    for i in range(5):
        listaTextos[i].undraw()
    listaTextos.clear()
    return