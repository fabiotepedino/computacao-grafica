# Projeto por Fabio Tepedino 

from aviao import * 
import time
import openpyxl
import numpy as np

# Utilizando a biblioteca "openpyxl.py" para ler o arquivo de coordenadas.
coordinates = openpyxl.load_workbook('plnilha de radar.xlsx', data_only=True)
sheet = coordinates.active

# Plota os avioes em determinado tempo
# O arquivo possui tempos de 0 a 150.
# A cada chamada, todos os avioes naquele tempo sao chamados
# Ex: Em tempo = 0, sao lidos somente as coordenadas que possuem tempo = 0. E depois plotados no buffer ativo
def PrintPlanesAtTempo(tempo):
    
    for i in range(1,97):
        row = 'A' + str(i)
        T = sheet[row]
        if T.value == tempo:

            Status = 'B' + str(i)
            Status = sheet[Status].value

            Voo = 'C' + str(i)
            Voo = sheet[Voo].value

            Distancia = 'D' + str(i)
            Distancia = sheet[Distancia].value

            Velocidade = 'E' + str(i)
            Velocidade = sheet[Velocidade].value

            X = 'F' + str(i)
            X = sheet[X].value

            Y = 'G' + str(i)
            Y = sheet[Y].value

            Z = 'H' + str(i)
            Z = sheet[Z].value

            # Apos coletar todos os dados de 1 aviao, envia-se as informacoes para a funcao que cria avioes.
            aviao(Status, Voo, Distancia, Velocidade, X, Y, Z)
    return
