# Projeto por Fabio Tepedino 

from Ponto import *

# Plota uma reta pontilhada no buffer ativo, via funcao Ponto()
def Reta_Pontilhada(x1,y1,x2,y2,espessura = 4, cor="white", incremento = 5):
    x = x1
    y = y1
    p = 0
    dX = x2-x1
    dY = y2-y1
    xInc = incremento
    yInc = incremento

    if dX<0 :
        xInc = -incremento; dX = -dX
    if dY<0 :
        yInc = -incremento; dY = -dY
    if dY <= dX:
        p = dX/2
        while x != x2:
            Ponto(x,y,espessura, cor)
            p=p-dY
            if p<0 :
                y = y+yInc
                p=p+dX
            x=x+xInc
    else:
        p = dY/2
        while y != y2:
            Ponto(x,y,espessura, cor)
            p = p-dX
            if p<0:
                x = x+xInc
                p = p+dY
            y = y+yInc
        Ponto(x,y,espessura, cor)    
    return

    