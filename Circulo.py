# Projeto por Fabio Tepedino 

from Ponto import*
import math

# Plota um circulo no buffer ativo, via funcao Ponto()
def Circulo(xc, yc, raio, espessura = 1, cor = "White"):
    x = 0
    y = raio
    p = 5/4-raio
    Ponto(x,y, espessura, cor)
    while x < y:
        x = x + 1
        if p<0:
            p = p + 2*x + 1
        else:
            y = y - 1
            p = p + 2*x + 1 - 2*y
        x = x+xc
        y = y+yc
        Ponto(x+xc, y+yc, espessura, cor )
        Ponto(y+xc, x+yc, espessura, cor )
        Ponto(y+xc, -x+yc, espessura, cor  )
        Ponto(-x+xc, y+yc, espessura, cor  )
        Ponto(-x+xc, -y+yc, espessura, cor  )
        Ponto(-y+xc, -x+yc, espessura, cor  )
        Ponto(-y+xc, x+yc, espessura, cor  )
        Ponto(x+xc, -y+yc, espessura, cor  )

    return

# Corrigi o valor para adaptar a biblioteca a um novo sistema de coordenadas, em que 0,0 fica no centro da tela
def correctionX(eixoX):
    return eixoX + sizeXWindow/2

# Corrigi o valor para adaptar a biblioteca a um novo sistema de coordenadas, em que 0,0 fica no centro da tela
def correctionY(eixoY):
    return sizeYWindow/2 - eixoY


        
    
